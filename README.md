# Godot Noisemark

A CPU-intensive benchmark of different Godot programming language alternatives. Similar in a sense to Carter Anderson's [Godot 3 Bunnymark](https://github.com/cart/godot3-bunnymark), but completely disregarding rendering and instead focused on processing.

This is basically just a measurement of the times taken by different implementations of [OpenSimplexNoise](http://uniblock.tumblr.com/post/97868843242/noise) -- all of them straightforward conversions from the reference Java code, without any significant optimizations.

The following implementations are benchmarked:

* Constant: Not a real implementation. This just uses a constant value as if it was the noise. This is meant to measure the overall overhead of looping through each image pixel and setting it.
* Built-in: Godot 3.1 got a built-in implementation of OpenSimplexNoise, and that's what we are measuring here. It's meant to be a lower bound: since it is compiled right into the engine executable, it doesn't have any overhead the other implementations do.
* GDNative (D): A D implementation, compiled to a GDNative module. I didn't measure this, but I'd assume that a GDNative module written in other languages would have a very similar performance (at least, they'd be qualitatively similar). I expect that the GDNative protocol itself introduces an overhead that would be larger than the difference between, say, C++, D, Nim and Rust. But again, this is just my guess.
* C#: 'nuff said.
* GDScript: Ditto!

## Results

I ran each of the implementations five times and took the average (I didn't took note of the standard deviation, but it was *really* low). The *Time − Constant* line is just this average subtracted of the *Constant* time (therefore it should be the most meaningful value: just the computation time, without the overhead of putting pixels into an image). The final column, *Relative to Built-in*, takes this latest value and scales it in proportion to the built-in implementation time, so that we have numbers that are easier to compare.

### Intel Core i7-8700 (8th Gen) @ 3.20GHz

| What             | Time (ms) | Time − Constant (ms) | Relative to Built-in |
| ---              | ---       | ---                  | ---                  |
| Constant         |   32.2    |    0.0               |  −                   |
| Built-in         |   98.6    |   66.4               |  1.00                |
| GDNative (D)     |  118.6    |   86.4               |  1.30                |
| C#               |  243.5    |  211.3               |  3.18                |
| GDScript         | 1509.8    | 1477.6               | 22.25                |

### Intel Core i5-4460 (4th Gen) @ 3.20GHz

| What             | Time (ms) | Time − Constant (ms) | Relative to Built-in |
| ---              | ---       | ---                  | ---                  |
| Constant         |   55.0    |    0.0               |  −                   |
| Built-in         |  139.6    |   84.6               |  1.00                |
| GDNative (D)     |  181.4    |  126.4               |  1.49                |
| C#               |  352.8    |  297.8               |  3.52                |
| GDScript         | 2152.4    | 2097.4               | 24.79                |

### Intel Core i3-8130U (8th Gen) @ 2.20GHz

| What             | Time (ms) | Time − Constant (ms) | Relative to Built-in |
| ---              | ---       | ---                  | ---                  |
| Constant         |   70.6    |    0.0               |  −                   |
| Built-in         |  146.8    |   76.2               |  1.00                |
| GDNative (D)     |  177.4    |  106.8               |  1.40                |
| C#               |  355.6    |  285.0               |  3.74                |
| GDScript         | 1946.4    | 1875.8               |  24.62               |

## Credits

I used code from different sources:

* GDScript: Jacob Guenther's [GDscriptOpenSimplexNoise](https://github.com/jacobguenther/GDscriptOpenSimplexNoise) ([Unlicensed](http://unlicense.org)).
* C#: Andrew Perry's [OpenSimplexNoise.cs](https://gist.github.com/omgwtfgames/601497972e4e30fd9c5f) (public domain)
* D: My own old [port of OpenSimplexNoise to D](https://github.com/lmbarros/sbxs_dlang/blob/master/src/sbxs/noise/open_simplex_noise.d) (MIT-licensed).

## Building

The only part that must be built is the D implementation (at the `gdnative-d` subdirectory). If you have a proper D development environment set up, it should just be a matter of running `dub build`. For the results above, I actually used `dub build --build=release --compiler=ldc` in order to have an optimized build using [LDC](https://github.com/ldc-developers/ldc), which generates faster code than [DMD](https://github.com/dlang/dmd).

## Notes and Caveats

* As any benchmark, take all this with a grain of salt.
* This was only tested on 64-bit Linux systems.
* Each of the timings above is actually the result of calling the noise function a total of 250 thousand times (to fill a 500×500 pixels image). Calling functions by itself has some overhead which, I'd guess, is not negligible in the case of GDNative. Hence, GDNative would probably be closer to the built-in implementation if I were calling a single long-running function once, instead of calling a relatively quick function lots of times. But this is just me guessing; some real measurements would be nice.
