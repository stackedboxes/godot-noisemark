extends Panel

const IMAGE_SIZE = 500

func _ready() -> void:
	randomize()



# - Constant value -------------------------------------------------------------
# Baseline measurement, probably very close to the GDScript "overhead" (looping
# and setting pixels)
func _on_ConstantButtonPressed() -> void:
	var img := Image.new()
	img.create(IMAGE_SIZE, IMAGE_SIZE, false, Image.FORMAT_L8)

	img.lock()
	var start := OS.get_ticks_msec()
	for x in range(IMAGE_SIZE):
		for y in range(IMAGE_SIZE):
			img.set_pixel(x, y, Color(0.8, 0.8, 0.8))
	var dt = OS.get_ticks_msec() - start
	img.unlock()

	var tex = ImageTexture.new()
	tex.create_from_image(img)
	$Preview.texture = tex

	$Constant/Time.text = str(dt) + " ms"



# - Built-in noise -------------------------------------------------------------
func _onBuiltInButtonPressed() -> void:
	var noise := OpenSimplexNoise.new()
	noise.seed = randi()
	noise.octaves = 1
	noise.period = 1

	var img := Image.new()
	img.create(IMAGE_SIZE, IMAGE_SIZE, false, Image.FORMAT_L8)

	img.lock()
	var start := OS.get_ticks_msec()
	for x in range(IMAGE_SIZE):
		for y in range(IMAGE_SIZE):
			var n := (noise.get_noise_2d(x, y) + 1.0) * 0.5
			img.set_pixel(x, y, Color(n, n, n))
	var dt = OS.get_ticks_msec() - start
	img.unlock()

	var tex = ImageTexture.new()
	tex.create_from_image(img)
	$Preview.texture = tex

	$BuiltIn/Time.text = str(dt) + " ms"



# - GDNative (D) ---------------------------------------------------------------
var GDNativeDOpenSimplexNoise := preload("res://OpenSimplexNoise.gdns")
func _onGDNativeDButtonPressed() -> void:
	var noise = GDNativeDOpenSimplexNoise.new()
	noise.seed(randi())

	var img := Image.new()
	img.create(IMAGE_SIZE, IMAGE_SIZE, false, Image.FORMAT_L8)

	img.lock()
	var start := OS.get_ticks_msec()
	for x in range(IMAGE_SIZE):
		for y in range(IMAGE_SIZE):
			var n: float = (noise.noise(x, y) + 1.0) * 0.5
			img.set_pixel(x, y, Color(n, n, n))
	var dt = OS.get_ticks_msec() - start
	img.unlock()

	var tex = ImageTexture.new()
	tex.create_from_image(img)
	$Preview.texture = tex

	$GDNativeD/Time.text = str(dt) + " ms"



# - C# -------------------------------------------------------------------------
var CSharpOpenSimplexNoise := preload("res://OpenSimplexNoise.cs")
func _onCSharpButtonPressed() -> void:
	var noise = CSharpOpenSimplexNoise.new()
	noise.seed(randi())

	var img := Image.new()
	img.create(IMAGE_SIZE, IMAGE_SIZE, false, Image.FORMAT_L8)

	img.lock()
	var start := OS.get_ticks_msec()
	for x in range(IMAGE_SIZE):
		for y in range(IMAGE_SIZE):
			var n: float = (noise.eval2D(x, y) + 1.0) * 0.5
			img.set_pixel(x, y, Color(n, n, n))
	var dt = OS.get_ticks_msec() - start
	img.unlock()

	var tex = ImageTexture.new()
	tex.create_from_image(img)
	$Preview.texture = tex

	$CSharp/Time.text = str(dt) + " ms"



# - GDScript -------------------------------------------------------------------
var GDScriptOpenSimplexNoise = preload("res://OpenSimplexNoise.gd")
func _onGDScriptButtonPressed() -> void:
	var noise = GDScriptOpenSimplexNoise.new()
	noise.setNewSeed(randi())

	var img := Image.new()
	img.create(IMAGE_SIZE, IMAGE_SIZE, false, Image.FORMAT_L8)

	img.lock()
	var start := OS.get_ticks_msec()
	for x in range(IMAGE_SIZE):
		for y in range(IMAGE_SIZE):
			var n: float = (noise.eval2D(x, y) + 1.0) * 0.5
			img.set_pixel(x, y, Color(n, n, n))
	var dt = OS.get_ticks_msec() - start
	img.unlock()

	var tex = ImageTexture.new()
	tex.create_from_image(img)
	$Preview.texture = tex

	$GDScript/Time.text = str(dt) + " ms"
