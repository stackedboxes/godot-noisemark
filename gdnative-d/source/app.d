// Build with: dub build --build=release --compiler=ldc

import godot, godot.node;

class OpenSimplexNoise: GodotScript!Node
{
	alias owner this;

    import std.math: sqrt;

    @Method
    public void seed(long seed)
    {
        // Initializes the struct using a permutation array generated from a
        // 64-bit seed.  Generates a proper permutation (i.e. doesn't merely
        // perform N successive pair swaps on a base array) Uses a simple 64-bit
        // LCG.
        short[256] source;
        foreach (short i; 0..256)
            source[i] = i;

        seed = seed * 6364136223846793005L + 1442695040888963407L;
        seed = seed * 6364136223846793005L + 1442695040888963407L;
        seed = seed * 6364136223846793005L + 1442695040888963407L;

        for (auto i = 255; i >= 0; --i)
        {
            seed = seed * 6364136223846793005L + 1442695040888963407L;
            int r = (seed + 31) % (i + 1);
            if (r < 0)
                r += (i + 1);
            _perm[i] = source[r];
            source[r] = source[i];
        }
    }

    /**
     * Computes and returns 2D noise.
     *
     * Parameters:
     *     x = The first coordinate from where noise will be taken.
     *     y = The second coordinate from where noise will be taken.
     *
     * Returns: The noise at the requested coordinates.
     */
    @Method
    public double noise(double x, double y) const
    {
        // Place input coordinates onto grid
        const double stretchOffset = (x + y) * _stretchConst2D;
        const double xs = x + stretchOffset;
        const double ys = y + stretchOffset;

        // Floor to get grid coordinates of rhombus (stretched square)
        // super-cell origin
        int xsb = fastFloor(xs);
        int ysb = fastFloor(ys);

        // Skew out to get actual coordinates of rhombus origin. We'll need
        // these later
        const double squishOffset = (xsb + ysb) * _squishConst2D;
        const double xb = xsb + squishOffset;
        const double yb = ysb + squishOffset;

        // Compute grid coordinates relative to rhombus origin
        const double xins = xs - xsb;
        const double yins = ys - ysb;

        // Sum those together to get a value that determines which region we're in
        const double inSum = xins + yins;

        // Positions relative to origin point
        double dx0 = x - xb;
        double dy0 = y - yb;

        // We'll be defining these inside the next block and using them afterwards
        double dx_ext, dy_ext;
        int xsv_ext, ysv_ext;

        double value = 0;

        // Contribution (1,0)
        const double dx1 = dx0 - 1 - _squishConst2D;
        const double dy1 = dy0 - 0 - _squishConst2D;
        double attn1 = 2 - dx1 * dx1 - dy1 * dy1;

        if (attn1 > 0)
        {
            attn1 *= attn1;
            value += attn1 * attn1 * extrapolate(xsb + 1, ysb + 0, dx1, dy1);
        }

        // Contribution (0,1)
        const double dx2 = dx0 - 0 - _squishConst2D;
        const double dy2 = dy0 - 1 - _squishConst2D;
        double attn2 = 2 - dx2 * dx2 - dy2 * dy2;
        if (attn2 > 0)
        {
            attn2 *= attn2;
            value += attn2 * attn2 * extrapolate(xsb + 0, ysb + 1, dx2, dy2);
        }

        if (inSum <= 1)
        {
            // We're inside the triangle (2-Simplex) at (0,0)
            const double zins = 1 - inSum;
            if (zins > xins || zins > yins)
            {
                // (0,0) is one of the closest two triangular vertices
                if (xins > yins)
                {
                    xsv_ext = xsb + 1;
                    ysv_ext = ysb - 1;
                    dx_ext = dx0 - 1;
                    dy_ext = dy0 + 1;
                }
                else
                {
                    xsv_ext = xsb - 1;
                    ysv_ext = ysb + 1;
                    dx_ext = dx0 + 1;
                    dy_ext = dy0 - 1;
                }
            }
            else
            {
                // (1,0) and (0,1) are the closest two vertices
                xsv_ext = xsb + 1;
                ysv_ext = ysb + 1;
                dx_ext = dx0 - 1 - 2 * _squishConst2D;
                dy_ext = dy0 - 1 - 2 * _squishConst2D;
            }
        }
        else
        {
            // We're inside the triangle (2-Simplex) at (1,1)
            const double zins = 2 - inSum;
            if (zins < xins || zins < yins)
            {
                // (0,0) is one of the closest two triangular vertices
                if (xins > yins)
                {
                    xsv_ext = xsb + 2;
                    ysv_ext = ysb + 0;
                    dx_ext = dx0 - 2 - 2 * _squishConst2D;
                    dy_ext = dy0 + 0 - 2 * _squishConst2D;
                }
                else
                {
                    xsv_ext = xsb + 0;
                    ysv_ext = ysb + 2;
                    dx_ext = dx0 + 0 - 2 * _squishConst2D;
                    dy_ext = dy0 - 2 - 2 * _squishConst2D;
                }
            }
            else
            {
                // (1,0) and (0,1) are the closest two vertices
                dx_ext = dx0;
                dy_ext = dy0;
                xsv_ext = xsb;
                ysv_ext = ysb;
            }
            xsb += 1;
            ysb += 1;
            dx0 = dx0 - 1 - 2 * _squishConst2D;
            dy0 = dy0 - 1 - 2 * _squishConst2D;
        }

        // Contribution (0,0) or (1,1)
        double attn0 = 2 - dx0 * dx0 - dy0 * dy0;
        if (attn0 > 0)
        {
            attn0 *= attn0;
            value += attn0 * attn0 * extrapolate(xsb, ysb, dx0, dy0);
        }

        // Extra Vertex
        double attn_ext = 2 - dx_ext * dx_ext - dy_ext * dy_ext;
        if (attn_ext > 0)
        {
            attn_ext *= attn_ext;
            value += attn_ext * attn_ext * extrapolate(xsv_ext, ysv_ext, dx_ext, dy_ext);
        }

        return value / _normConst2D;
    }

    //
    // Helpers
    //

    /**
     * Computes the floor of a given number.
     *
     * This is way faster than simply calling `std.math.floor()` and casting to
     * an `int`. I (LMB) did a couple of benchmarks with DMD 2.060, using flags
     * `-O -inline`. Overall, noise generation was almost 10% faster when using
     * `fastFloor()` instead of `std.math.floor()`.
     */
    private pure nothrow int fastFloor(double x) const
    {
        return x > 0
            ? cast(int)(x)
            : cast(int)(x-1);
    }

    private nothrow double extrapolate(int xsb, int ysb, double dx, double dy) const
    {
        const ix = xsb & 0xFF;
        const iy = (_perm[ix] + ysb) & 0xFF;
        const index = _perm[iy] & 0x0E;

        return _gradients2D[index + 0]  * dx
            + _gradients2D[index + 1]  * dy;
    }

    //
    // The noise generator state
    //
    private short[256] _perm;


    //
    // Assorted constants used throughout the code
    //
    private enum
    {
        _stretchConst2D = (1 / sqrt(2+1.0) - 1) / 2,
        _stretchConst3D = (1 / sqrt(3+1.0) - 1) / 3,
        _stretchConst4D = (1 / sqrt(4+1.0) - 1) / 4,

        _squishConst2D = (sqrt(2+1.0) - 1) / 2,
        _squishConst3D = (sqrt(3+1.0) - 1) / 3,
        _squishConst4D = (sqrt(4+1.0) - 1) / 4,

        _normConst2D = 47.0,
        _normConst3D = 103.0,
        _normConst4D = 30.0,

        /**
         * Gradients for 2D. They approximate the directions to the vertices
         * of an octagon from the center.
         */
        _gradients2D = [
             5,  2,      2,  5,
            -5,  2,     -2,  5,
             5, -2,      2, -5,
            -5, -2,     -2, -5 ],
    }
}


mixin GodotNativeLibrary!
(
	"godot_",
	OpenSimplexNoise,
);
